// import {Fragment} from 'react'
import {useState, useEffect} from 'react'
import {Container} from 'react-bootstrap'
import{BrowserRouter as Router} from 'react-router-dom'
import{Route, Switch} from 'react-router-dom'
import AppNavbar from './components/AppNavbar'
// import Banner from './components/Banner'
import NotFound from './components/NotFound'
import CourseView from './pages/CourseView'
// import Highlights from './components/Highlights'
import Courses from './pages/Courses'
import Home from './pages/Home'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Register from './pages/Register'
import './App.css';
import {UserProvider} from './UserContext'

function App() {

  //State hook for the user state thats defined here for a global scope 
  //INitialized as an object with properties from the locaStorage
  //This will be used to store the info and will be used for validating if auser logged in on the app or not
  const [user,setUser]= useState({
    // email:localStorage.getItem('email')
    id:null,
    isAdmin:null
  })

  //function to clear local storage
  const unsetUser = () =>{
    localStorage.clear()
  }

  useEffect(()=>{
    let token = localStorage.getItem('token');
    fetch('http://localhost:4000/users/details',{
      method:'GET',
      headers: {
        Authorization:`Bearer ${token}`
      }
    })
    .then(res=>res.json())
    .then(data=>{
      console.log(data)

      if (typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }else{
        setUser({
          id:null,
          isAdmin: null
        })
      }
    })
  },[])

// The UserProvider component is what allows other components to consume or use our context. Any component which is not wrapped by userProvider will not have access to the values provided for our context

//You can pass data or information to our context by providing a "value" attribute in our UserProvider. Data passed here can be accessed by other components by unwrapping our context using the useContext hook. 


  return (

    <UserProvider value ={{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar/>
      
      <Container>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route  path= "/courses"component={Courses}/>
          <Route exact path="/login" component={Login}/>
         {/* <Route exact path = "/courseView" element ={CourseView}/>*/}
          <Route exact path="/courses/:courseId" component={CourseView}/>
          <Route exact path="/register" component={Register}/>
          <Route exact path="/logout" component={Logout}/>
          <Route component={NotFound} />
                
        </Switch>
      </Container>
    </Router>
    </UserProvider>



  );
}

export default App;


/*
RreactJs is a single page application (SPA). However, we can simulate the changing of pages, We don't actually create pagesm what we just do is switch pages according to their assigned routes. React.js and react-router-dom package just mimics or mirrors how HTML access its URL.

react-router-dom 3 main components to simulate the changing of page

1. Router - wrapping the router component arount other components will allow us to use routing within our page.
2.Switch - Allow us to switch or change our page components.
3. Route- Assigns a path which will trigger the change/switch or components render.

  <Fragment>
    // <AppNavbar/>
    // <Container>    
      <Banner/>
      <Highlights/>
      <Register/>
      // <Login/>
      <Home/>
      <Courses/>
    // </Container>
  // </Fragment>


*/
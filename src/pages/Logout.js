import {Redirect} from 'react-router-dom'
import {useEffect,useContext} from 'react'
import UserContext from '../UserContext'

export default function Logout(){

	//consume the UserContext object and destructure it to access the user state and unsetUser function from the context provider.
	const {unsetUser, setUser} = useContext(UserContext)
	localStorage.clear();


	//clear the localStorage of the users info
	unsetUser();

	//Placing the 'setuser' setter function inside the useeffect is necessary vecause of the updates within react JS that a state of another component cannot be updated while trying to render a different component

	//By adding the useEffect, this will allow the logout page to render first before triggering the useEffect which changes the state of our user.
	useEffect(()=> {
		//set user state back to its original value
		setUser({id:null})
	})
	//Redirect allows us to navigate to a new location
	return(

		<Redirect to='/login' />

		)
}
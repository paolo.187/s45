import {useState, useEffect} from 'react'
import {useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import {Redirect, useHistory} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


export default function Register (){

	const history = useHistory()
	const {user,setUser} = useContext(UserContext)
	console.log(user)
	//State hooks to store the values of the input fields
	const [firstName,setFirstName] = useState('')
	const [lastName,setLastName] = useState('')
	const [mobile,setMobile] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [email, setEmail] = useState('')

	//State to determine wheather to register button is enabled or not
	const [isActive, setIsActive]= useState(false)

	console.log(firstName)
	console.log(lastName)
	console.log(mobile)
	console.log(email)
	console.log(password1)
	console.log(password2)

	function registerUser(e){

		//prevents page redirection via form submission
		e.preventDefault()

		fetch('http://localhost:4000/users/checkEmail',{
			method: 'POST',
			headers:{
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email:email
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)

			if (data == false){


				fetch('http://localhost:4000/users/register',{
					method: 'POST',
					headers:{
						'Content-Type':'application/json'
					},
					body: JSON.stringify({
						firstName :firstName,
						lastName : lastName,
						email : email,
						mobileNo : mobile,
						password : password1
					})

				})
				.then(res=>res.json())
				.then(data=>{
					console.log(data)
				})

				setEmail('');
				setPassword1('');
				setPassword2('')

				Swal.fire({
					title:'Registration Successful!',
					icon:'success',
					text: 'welcome to Zuitt!'
				})
		
				history.push("/login")

			}else{
				setEmail('');
				Swal.fire({
					title:'Cannot Register',
					icon:'error',
					text: 'email already exists'
				})
				history.push("/register")
			}
		})
		
		

		

	}




	useEffect(()=>{
		let lengthOfMobile = mobile.length
		if ((firstName !=='' && lastName !=='' && mobile !=='' && email !=='' && password1 !=='' && password2 !=='')&& (password1 === password2 )&&(lengthOfMobile == 11)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[firstName,lastName,mobile,email,password1,password2])

	return(


		// S49 solution
			//make a if statment to check if user info is stored from login (userContext), if true, redirect to /courses
			//import Redirect from react-router-dom
			//setup as destructured useContext (const)
			//import UserContext.js
			//import useContext from 'react'

		(user.id !== null)?
				<Redirect to='/courses'/>

				:

			<Form onSubmit={(e)=>registerUser(e)}>
				<Form.Group>
					<Form.Label>First Name:</Form.Label>
					<Form.Control
						type= 'namespace'
						placeholder = 'Please enter your firstname here'
						value={firstName}
						onChange = {e =>setFirstName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Last Name:</Form.Label>
					<Form.Control
						type= 'namespace'
						placeholder = 'Please enter your Last Name here'
						value={lastName}
						onChange = {e =>setLastName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Mobile Number:</Form.Label>
					<Form.Control
						type= 'namespace'
						placeholder = 'Please enter your 11- digit mobile number'
						value={mobile}
						onChange = {e =>setMobile(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Email Adress:</Form.Label>
					<Form.Control
						type= 'email'
						placeholder = 'Please enter your email here'
						value={email}
						onChange = {e =>setEmail(e.target.value)}
						required
					/>
					<Form.Text className ="text-muted">
						We will never share your email with anyone else.
					</Form.Text>
				</Form.Group>

				<Form.Group controlId = "password1">
						<Form.Label>Password:</Form.Label>
						<Form.Control
							type = 'password'
							placeholder = "Please input your password here"
							value ={password1}
							onChange = {e => setPassword1(e.target.value)}
							required
							/>
				</Form.Group>

				<Form.Group controlId='password2'>
					<Form.Label>Verify Password:</Form.Label>
					<Form.Control
						type = 'password'
						placeholder = 'Please Verify password'
						value ={password2}
							onChange = {e => setPassword2(e.target.value)}
						required
					/>
				</Form.Group>

				{ isActive ? 
					<Button variant = 'primary' type='submit' id="submitBtn">Register
					</Button>

					:
					<Button variant ='danger' type = 'submit' id = 'submitBtn' disabled>Register</Button>
				}
			</Form>

		)
}
/*
Ternary Operator with the Button tag

? -if -- if button is active , the color wlill be primary and is enabled
: -else-- button will be color danger and will be disabled*/

/*useEffect - react hook
Syntax: useEffect(()=>{
	<conditions>
},[parameter])
-- once a change happens in the parameter, the useEffect will run*/